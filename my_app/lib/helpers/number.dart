import 'dart:math';

double radiansToDegrees(double radians) {
  return (radians * 180) / pi;
}

bool approximatelyEqual(double number1, double number2,
    [double epsilon = 0.5]) {
  double diff = (number1 - number2).abs();
  return diff <= epsilon;
}
