void safeWhile(bool Function() condition, bool? Function() body) {
  num stepCounter = 0;
  while (condition()) {
    bool? needBreak = body();
    if (needBreak == true) {
      break;
    }
    stepCounter++;
    if (stepCounter > 999) {
      throw Exception('while step overflow');
    }
  }
}
