import 'package:my_app/helpers/guid.dart';
import 'package:my_app/models/point.dart';

String getIdFromPoints(String prefix, List<Point> points) {
  if (points.isNotEmpty) {
    return '$prefix-${points.map((Point point) => '${point.x.toStringAsFixed(0)}-${point.y.toStringAsFixed(0)}').join('--')}';
  } else {
    return '$prefix-${GUID.generate()}';
  }
}

Point getCenter(List<Point> points) {
  double x = 0;
  double y = 0;

  for (var point in points) {
    x += point.x;
    y += point.y;
  }

  return Point(x: x / points.length, y: y / points.length);
}
