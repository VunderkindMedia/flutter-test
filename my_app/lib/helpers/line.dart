import 'package:my_app/models/line.dart';

T? getBottommostLine<T extends Line>(List<T> lines) {
  List<T> localLines =
      lines.sublist(0).where((T line) => line.OX.length != 0).toList();

  localLines.sort(
      (T l1, T l2) => l1.rect.center.y.toInt() - l2.rect.center.y.toInt());

  if (localLines.isEmpty) {
    return null;
  }
  return localLines[0];
}
