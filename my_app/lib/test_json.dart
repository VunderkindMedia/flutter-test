Map<String, dynamic> construction = {
  "amegaCost": 2805000,
  "amegaCostToDisplay": 2805000,
  "calculatedWithFramerPoints": false,
  "connectorsParameters": [
    {
      "a": 30,
      "b": null,
      "c": null,
      "comment": "Расширитель 30 мм",
      "connectorType": "extender",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 296,
      "isDefault": false,
      "marking": "144",
      "modelPart": 4
    },
    {
      "a": 120,
      "b": null,
      "c": null,
      "comment": "Расширитель 120 мм",
      "connectorType": "extender",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 293,
      "isDefault": false,
      "marking": "147",
      "modelPart": 4
    },
    {
      "a": 5,
      "b": null,
      "c": null,
      "comment": "Соединитель Н-образный",
      "connectorType": "connector",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 290,
      "isDefault": false,
      "marking": "150",
      "modelPart": 4
    },
    {
      "a": 20,
      "b": null,
      "c": null,
      "comment": "Соединитель Н-образный с армированием",
      "connectorType": "connector",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 291,
      "isDefault": false,
      "marking": "152",
      "modelPart": 4
    },
    {
      "a": 149,
      "b": null,
      "c": null,
      "comment": "Соединитель угловой 90 градусов",
      "connectorType": "connector",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 292,
      "isDefault": false,
      "marking": "155",
      "modelPart": 4
    },
    {
      "a": 182,
      "b": null,
      "c": null,
      "comment": "Соединитель с переменным углом",
      "connectorType": "connector",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 553,
      "isDefault": false,
      "marking": "540+541",
      "modelPart": 4
    },
    {
      "a": 45,
      "b": null,
      "c": null,
      "comment": "Расширитель 45 мм",
      "connectorType": "extender",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 295,
      "isDefault": false,
      "marking": "545",
      "modelPart": 4
    },
    {
      "a": 60,
      "b": null,
      "c": null,
      "comment": "Расширитель 60 мм",
      "connectorType": "extender",
      "d": null,
      "e": null,
      "f": null,
      "g": null,
      "id": 294,
      "isDefault": false,
      "marking": "546",
      "modelPart": 4
    }
  ],
  "coordinateSystem": "relative",
  "defaultPointOfView": "inside",
  "energyProductType": "",
  "errors": [
    {
      "beamGuids": null,
      "errorAdditionalInfo": "Кривая рама",
      "errorCode": "и066",
      "errorMessage": "Причина нестандарта",
      "errorName": "Причина нестандарта",
      "errorType": "Информация",
      "positionName": "2. Окно KBE Эталон 58 (Фурнитура Maco MM)",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    },
    {
      "beamGuids": null,
      "errorAdditionalInfo": "Заполнение непрямоугольной формы",
      "errorCode": "и066",
      "errorMessage": "Причина нестандарта",
      "errorName": "Причина нестандарта",
      "errorType": "Информация",
      "positionName": "2. 4-10-4-10-4",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    },
    {
      "beamGuids": null,
      "errorAdditionalInfo": "Заполнение непрямоугольной формы",
      "errorCode": "и066",
      "errorMessage": "Причина нестандарта",
      "errorName": "Причина нестандарта",
      "errorType": "Информация",
      "positionName": "2. 4-10-4-10-4",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    },
    {
      "beamGuids": null,
      "errorAdditionalInfo": "Коэффициент сопротивления = 0,5207",
      "errorCode": "и611",
      "errorMessage": "",
      "errorName": "Коэффициент сопротивления теплопередачи",
      "errorType": "Информация",
      "positionName": "2. Окно KBE Эталон 58 (Фурнитура Maco MM)",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    },
    {
      "beamGuids": null,
      "errorAdditionalInfo": "",
      "errorCode": "и225",
      "errorMessage":
          "В створке с поворотным режимом открывания между петлями будут установлены средние (внешние) прижимы",
      "errorName":
          "В створке с поворотным режимом открывания между петлями будут установлены средние (внешние) прижимы",
      "errorType": "Информация",
      "positionName": "2. Створка KBE Эталон 58 (Фурнитура Maco MM)",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    },
    {
      "beamGuids": null,
      "errorAdditionalInfo": "",
      "errorCode": "и937",
      "errorMessage":
          "В поворотном режиме открывания при стандартной комплектации фурнитуры возможно продувание створки. Предупредите заказчика или рассчитайте средний запор принудительно",
      "errorName":
          "В поворотном режиме открывания при стандартной комплектации фурнитуры возможно продувание створки. Предупредите заказчика или рассчитайте средний запор принудительно",
      "errorType": "Информация",
      "positionName": "2. Створка KBE Эталон 58 (Фурнитура Maco MM)",
      "productionGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "showInModal": false
    }
  ],
  "estShippingDate": "2021-07-31T00:00:00Z",
  "hasErrors": false,
  "haveEnergyProducts": false,
  "id": "28eb4db1-f62c-5b04-a94e-21f3e9bca504",
  "name": "",
  "position": 1,
  "products": [
    {
      "connectors": [],
      "constructionTypeId": 2,
      "frame": {
        "fillingPoint": {"x": 1000, "y": 793.3333},
        "frameBeams": [
          {
            "beamGuid": "c96cae5c-0eae-052f-4f16-2f1b60d2b4fd",
            "end": {"x": 0, "y": 0},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 2000, "y": 0},
            "userParameters": []
          },
          {
            "beamGuid": "a9ec7959-97f8-d6d0-aec8-38b14f2f5d7d",
            "end": {"x": 0, "y": 980},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 0, "y": 0},
            "userParameters": []
          },
          {
            "beamGuid": "a40573da-9daf-7dff-7a8b-ad9c9c5973be",
            "end": {"x": 370, "y": 1400},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 0, "y": 980},
            "userParameters": []
          },
          {
            "beamGuid": "35c71b99-ca58-865b-f301-04536ef1f2fe",
            "end": {"x": 1630, "y": 1400},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 370, "y": 1400},
            "userParameters": []
          },
          {
            "beamGuid": "27bc8244-ba55-3a8d-67ed-ff6cca24a1ea",
            "end": {"x": 2000, "y": 980},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 1630, "y": 1400},
            "userParameters": []
          },
          {
            "beamGuid": "108551e8-d593-6d9b-bc3e-855d5c872bce",
            "end": {"x": 2000, "y": 0},
            "hardcodedUserParameters": [],
            "modelPart": 1,
            "profileVendorCode": "807",
            "start": {"x": 2000, "y": 980},
            "userParameters": []
          }
        ],
        "frameFillingGuid": "b9c48db8-1391-1f67-f616-65f17460cbdf",
        "furniture": {
          "displayName": "Maco MM",
          "id": 27,
          "isDefault": false,
          "isEmpty": false,
          "name": "Фурнитура Maco MM",
          "vars":
              "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701"
        },
        "handleColor": "Белый",
        "handlePositionType": "center",
        "handleType": "МакБет",
        "hardcodedUserParameters": [],
        "impostBeams": [
          {
            "beamGuid": "5fc913d5-f255-e541-c75a-2612b2b1d63f",
            "end": {"x": 667, "y": 1400},
            "hardcodedUserParameters": [],
            "modelPart": 3,
            "profileVendorCode": "337",
            "shtulpType": 3,
            "start": {"x": 667, "y": 0},
            "userParameters": []
          },
          {
            "beamGuid": "3288aa3c-829c-f8db-efd4-e36e9a5b165b",
            "end": {"x": 1333, "y": 1400},
            "hardcodedUserParameters": [],
            "modelPart": 3,
            "profileVendorCode": "337",
            "shtulpType": 3,
            "start": {"x": 1333, "y": 0},
            "userParameters": []
          }
        ],
        "innerFillings": [
          {
            "solid": {
              "filling": {
                "camernost": 2,
                "group": "Стеклопакеты",
                "id": 27,
                "isDefault": true,
                "isLowEmission": false,
                "isMultifunctional": false,
                "marking": "4-10-4-10-4",
                "name": "Стеклопакет 32 мм",
                "thickness": 32
              },
              "fillingPoint": {"x": 340.8, "y": 756},
              "fillingSize": {
                "bevels": [
                  {"x": 344, "y": 390}
                ],
                "size": {"x": 590, "y": 1304}
              },
              "fillingVendorCode": "4-10-4-10-4",
              "hardcodedUserParameters": [],
              "solidFillingGuid": "37070eba-d3f7-98b5-7226-eebb0abb7942",
              "userParameters": [
                {"hex": "", "id": 1099, "title": "", "value": "Нет"}
              ]
            }
          },
          {
            "leaf": {
              "fillingPoint": {"x": 1000, "y": 700},
              "frameBeams": [
                {
                  "beamGuid": "4bf20bd2-40c6-88b7-03cf-5f0723e64112",
                  "end": {"x": 683, "y": 35},
                  "hardcodedUserParameters": [],
                  "modelPart": 2,
                  "profileVendorCode": "817_04",
                  "start": {"x": 1317, "y": 35},
                  "userParameters": []
                },
                {
                  "beamGuid": "da87511d-2989-60c5-93b7-bbaaf974adf7",
                  "end": {"x": 683, "y": 1365},
                  "hardcodedUserParameters": [],
                  "modelPart": 2,
                  "profileVendorCode": "817_04",
                  "start": {"x": 683, "y": 35},
                  "userParameters": []
                },
                {
                  "beamGuid": "c7037f72-86fb-7478-7baa-8aa851f582df",
                  "end": {"x": 1317, "y": 1365},
                  "hardcodedUserParameters": [],
                  "modelPart": 2,
                  "profileVendorCode": "817_04",
                  "start": {"x": 683, "y": 1365},
                  "userParameters": []
                },
                {
                  "beamGuid": "4896ec73-0796-39db-7d07-517e30fd9495",
                  "end": {"x": 1317, "y": 35},
                  "hardcodedUserParameters": [],
                  "modelPart": 2,
                  "profileVendorCode": "817_04",
                  "start": {"x": 1317, "y": 1365},
                  "userParameters": []
                }
              ],
              "frameFillingGuid": "e6df4324-c38a-1fe5-6a90-9642b01dac69",
              "furniture": {
                "displayName": "Maco MM",
                "id": 27,
                "isDefault": false,
                "isEmpty": false,
                "name": "Фурнитура Maco MM",
                "vars":
                    "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701"
              },
              "handleColor": "Белый",
              "handlePositionType": "center",
              "handleType": "МакБет",
              "hardcodedUserParameters": null,
              "impostBeams": [],
              "innerFillings": [
                {
                  "solid": {
                    "filling": {
                      "camernost": 2,
                      "group": "Стеклопакеты",
                      "id": 27,
                      "isDefault": true,
                      "isLowEmission": false,
                      "isMultifunctional": false,
                      "marking": "4-10-4-10-4",
                      "name": "Стеклопакет 32 мм",
                      "thickness": 32
                    },
                    "fillingPoint": {"x": 1000, "y": 700},
                    "fillingSize": {
                      "bevels": [],
                      "size": {"x": 511, "y": 1206}
                    },
                    "fillingVendorCode": "4-10-4-10-4",
                    "hardcodedUserParameters": [],
                    "solidFillingGuid": "9de63ce3-be8b-d5d0-5d62-ad5303a29c27",
                    "userParameters": [
                      {"hex": "", "id": 1099, "title": "", "value": "Нет"},
                      {
                        "hex": "",
                        "id": 2698,
                        "title": "Вклейка стеклопакета",
                        "value": "Нет"
                      }
                    ]
                  }
                }
              ],
              "moskitka": {"catproof": false, "color": "white", "set": false},
              "moskitkaSize": null,
              "openSide": 1,
              "openType": 1,
              "profile": {
                "allowedThickness": [0, 4, 5, 6, 24, 28, 32],
                "brand": "KBE",
                "camernost": 3,
                "class": "Б",
                "displayName": "КБЕ 58мм",
                "id": 16,
                "isDefault": false,
                "isEmpty": false,
                "isEnergy": null,
                "isMoskitka": false,
                "name": "KBE Эталон 58",
                "parts": [
                  {
                    "a": 26,
                    "b": null,
                    "c": null,
                    "comment": "Фальш-переплет на белой основе",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 287,
                    "marking": "PR SP 751",
                    "modelPart": 8
                  },
                  {
                    "a": 63,
                    "b": 0,
                    "c": 43,
                    "comment": "Оконная рама",
                    "d": 0,
                    "e": null,
                    "f": 0,
                    "g": 0,
                    "id": 310,
                    "marking": "807",
                    "modelPart": 1
                  },
                  {
                    "a": 77,
                    "b": 0,
                    "c": 57,
                    "comment": "Створка оконная 77мм",
                    "d": 0,
                    "e": 20,
                    "f": 0,
                    "g": 0,
                    "id": 308,
                    "marking": "817_04",
                    "modelPart": 2
                  },
                  {
                    "a": 5,
                    "b": null,
                    "c": null,
                    "comment": "Соединитель Н-образный",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 290,
                    "marking": "150",
                    "modelPart": 4
                  },
                  {
                    "a": 44,
                    "b": null,
                    "c": 24,
                    "comment": "Импост оконный",
                    "d": 0,
                    "e": null,
                    "f": 0,
                    "g": 6,
                    "id": 298,
                    "marking": "337",
                    "modelPart": 3
                  },
                  {
                    "a": 32,
                    "b": null,
                    "c": 12,
                    "comment": "Штульп оконный",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 299,
                    "marking": "734",
                    "modelPart": 6
                  },
                  {
                    "a": 20,
                    "b": null,
                    "c": null,
                    "comment": "Соединитель Н-образный с армированием",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 291,
                    "marking": "152",
                    "modelPart": 4
                  },
                  {
                    "a": 149,
                    "b": null,
                    "c": null,
                    "comment": "Соединитель угловой 90 градусов",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 292,
                    "marking": "155",
                    "modelPart": 4
                  },
                  {
                    "a": 182,
                    "b": null,
                    "c": null,
                    "comment": "Соединитель с переменным углом",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 553,
                    "marking": "540+541",
                    "modelPart": 4
                  },
                  {
                    "a": 30,
                    "b": null,
                    "c": null,
                    "comment": "Расширитель 30 мм",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 296,
                    "marking": "144",
                    "modelPart": 4
                  },
                  {
                    "a": 45,
                    "b": null,
                    "c": null,
                    "comment": "Расширитель 45 мм",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 295,
                    "marking": "545",
                    "modelPart": 4
                  },
                  {
                    "a": 60,
                    "b": null,
                    "c": null,
                    "comment": "Расширитель 60 мм",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 294,
                    "marking": "546",
                    "modelPart": 4
                  },
                  {
                    "a": 120,
                    "b": null,
                    "c": null,
                    "comment": "Расширитель 120 мм",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 293,
                    "marking": "147",
                    "modelPart": 4
                  },
                  {
                    "a": 50,
                    "b": null,
                    "c": null,
                    "comment": "Соединитель не определен",
                    "d": null,
                    "e": null,
                    "f": null,
                    "g": null,
                    "id": 754,
                    "marking": "Соединитель не определен",
                    "modelPart": 4
                  }
                ],
                "thickness": 58
              },
              "shtulpOpenType": 0,
              "userParameters": [
                {
                  "hex": "",
                  "id": 413,
                  "title": "Петли оконные",
                  "value": "Обычные"
                },
                {
                  "hex": "",
                  "id": 416,
                  "title": "Средний запор для Масо",
                  "value": "Авто"
                }
              ]
            }
          },
          {
            "solid": {
              "filling": {
                "camernost": 2,
                "group": "Стеклопакеты",
                "id": 27,
                "isDefault": true,
                "isLowEmission": false,
                "isMultifunctional": false,
                "marking": "4-10-4-10-4",
                "name": "Стеклопакет 32 мм",
                "thickness": 32
              },
              "fillingPoint": {"x": 1659.2, "y": 756},
              "fillingSize": {
                "bevels": [
                  {"x": 344, "y": 390}
                ],
                "size": {"x": 590, "y": 1304}
              },
              "fillingVendorCode": "4-10-4-10-4",
              "hardcodedUserParameters": [],
              "solidFillingGuid": "4f0536a2-f3a6-c73d-8120-9d09bc344bd3",
              "userParameters": [
                {"hex": "", "id": 1099, "title": "", "value": "Нет"}
              ]
            }
          }
        ],
        "moskitka": {"catproof": false, "color": "white", "set": false},
        "openSide": 4,
        "openType": 0,
        "profile": {
          "allowedThickness": [0, 4, 5, 6, 24, 28, 32],
          "brand": "KBE",
          "camernost": 3,
          "class": "Б",
          "displayName": "КБЕ 58мм",
          "id": 16,
          "isDefault": false,
          "isEmpty": false,
          "isEnergy": null,
          "isMoskitka": false,
          "name": "KBE Эталон 58",
          "parts": [
            {
              "a": 26,
              "b": null,
              "c": null,
              "comment": "Фальш-переплет на белой основе",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 287,
              "marking": "PR SP 751",
              "modelPart": 8
            },
            {
              "a": 63,
              "b": 0,
              "c": 43,
              "comment": "Оконная рама",
              "d": 0,
              "e": null,
              "f": 0,
              "g": 0,
              "id": 310,
              "marking": "807",
              "modelPart": 1
            },
            {
              "a": 77,
              "b": 0,
              "c": 57,
              "comment": "Створка оконная 77мм",
              "d": 0,
              "e": 20,
              "f": 0,
              "g": 0,
              "id": 308,
              "marking": "817_04",
              "modelPart": 2
            },
            {
              "a": 5,
              "b": null,
              "c": null,
              "comment": "Соединитель Н-образный",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 290,
              "marking": "150",
              "modelPart": 4
            },
            {
              "a": 44,
              "b": null,
              "c": 24,
              "comment": "Импост оконный",
              "d": 0,
              "e": null,
              "f": 0,
              "g": 6,
              "id": 298,
              "marking": "337",
              "modelPart": 3
            },
            {
              "a": 32,
              "b": null,
              "c": 12,
              "comment": "Штульп оконный",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 299,
              "marking": "734",
              "modelPart": 6
            },
            {
              "a": 20,
              "b": null,
              "c": null,
              "comment": "Соединитель Н-образный с армированием",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 291,
              "marking": "152",
              "modelPart": 4
            },
            {
              "a": 149,
              "b": null,
              "c": null,
              "comment": "Соединитель угловой 90 градусов",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 292,
              "marking": "155",
              "modelPart": 4
            },
            {
              "a": 182,
              "b": null,
              "c": null,
              "comment": "Соединитель с переменным углом",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 553,
              "marking": "540+541",
              "modelPart": 4
            },
            {
              "a": 30,
              "b": null,
              "c": null,
              "comment": "Расширитель 30 мм",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 296,
              "marking": "144",
              "modelPart": 4
            },
            {
              "a": 45,
              "b": null,
              "c": null,
              "comment": "Расширитель 45 мм",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 295,
              "marking": "545",
              "modelPart": 4
            },
            {
              "a": 60,
              "b": null,
              "c": null,
              "comment": "Расширитель 60 мм",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 294,
              "marking": "546",
              "modelPart": 4
            },
            {
              "a": 120,
              "b": null,
              "c": null,
              "comment": "Расширитель 120 мм",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 293,
              "marking": "147",
              "modelPart": 4
            },
            {
              "a": 50,
              "b": null,
              "c": null,
              "comment": "Соединитель не определен",
              "d": null,
              "e": null,
              "f": null,
              "g": null,
              "id": 754,
              "marking": "Соединитель не определен",
              "modelPart": 4
            }
          ],
          "thickness": 58
        },
        "shtulpOpenType": 0,
        "userParameters": []
      },
      "furniture": {
        "displayName": "Maco MM",
        "id": 27,
        "isDefault": false,
        "isEmpty": false,
        "name": "Фурнитура Maco MM",
        "vars":
            "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701"
      },
      "hardcodedUserParameters": [
        {"hex": "", "id": 1878, "title": "Бренд", "value": "Амега Пермь"},
        {
          "hex": "",
          "id": 2831,
          "title": "Крепление импоста ПВХ",
          "value": "Металлическое"
        }
      ],
      "hardwareSystemName": "Фурнитура Maco MM",
      "heatTransferResistanceCoef": 0.5207,
      "insideColorId": 2,
      "insideLamination": {"hex": "#fff", "id": 2, "name": "Белый"},
      "outsideColorId": 2,
      "outsideLamination": {"hex": "#fff", "id": 2, "name": "Белый"},
      "productGuid": "65a2879c-74be-d4a4-d2f1-962c4866473b",
      "productOffset": 0,
      "productionTypeId": 1194,
      "profile": {
        "allowedThickness": [0, 4, 5, 6, 24, 28, 32],
        "brand": "KBE",
        "camernost": 3,
        "class": "Б",
        "displayName": "КБЕ 58мм",
        "id": 16,
        "isDefault": false,
        "isEmpty": false,
        "isEnergy": null,
        "isMoskitka": false,
        "name": "KBE Эталон 58",
        "parts": [
          {
            "a": 26,
            "b": null,
            "c": null,
            "comment": "Фальш-переплет на белой основе",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 287,
            "marking": "PR SP 751",
            "modelPart": 8
          },
          {
            "a": 63,
            "b": 0,
            "c": 43,
            "comment": "Оконная рама",
            "d": 0,
            "e": null,
            "f": 0,
            "g": 0,
            "id": 310,
            "marking": "807",
            "modelPart": 1
          },
          {
            "a": 77,
            "b": 0,
            "c": 57,
            "comment": "Створка оконная 77мм",
            "d": 0,
            "e": 20,
            "f": 0,
            "g": 0,
            "id": 308,
            "marking": "817_04",
            "modelPart": 2
          },
          {
            "a": 5,
            "b": null,
            "c": null,
            "comment": "Соединитель Н-образный",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 290,
            "marking": "150",
            "modelPart": 4
          },
          {
            "a": 44,
            "b": null,
            "c": 24,
            "comment": "Импост оконный",
            "d": 0,
            "e": null,
            "f": 0,
            "g": 6,
            "id": 298,
            "marking": "337",
            "modelPart": 3
          },
          {
            "a": 32,
            "b": null,
            "c": 12,
            "comment": "Штульп оконный",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 299,
            "marking": "734",
            "modelPart": 6
          },
          {
            "a": 20,
            "b": null,
            "c": null,
            "comment": "Соединитель Н-образный с армированием",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 291,
            "marking": "152",
            "modelPart": 4
          },
          {
            "a": 149,
            "b": null,
            "c": null,
            "comment": "Соединитель угловой 90 градусов",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 292,
            "marking": "155",
            "modelPart": 4
          },
          {
            "a": 182,
            "b": null,
            "c": null,
            "comment": "Соединитель с переменным углом",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 553,
            "marking": "540+541",
            "modelPart": 4
          },
          {
            "a": 30,
            "b": null,
            "c": null,
            "comment": "Расширитель 30 мм",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 296,
            "marking": "144",
            "modelPart": 4
          },
          {
            "a": 45,
            "b": null,
            "c": null,
            "comment": "Расширитель 45 мм",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 295,
            "marking": "545",
            "modelPart": 4
          },
          {
            "a": 60,
            "b": null,
            "c": null,
            "comment": "Расширитель 60 мм",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 294,
            "marking": "546",
            "modelPart": 4
          },
          {
            "a": 120,
            "b": null,
            "c": null,
            "comment": "Расширитель 120 мм",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 293,
            "marking": "147",
            "modelPart": 4
          },
          {
            "a": 50,
            "b": null,
            "c": null,
            "comment": "Соединитель не определен",
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "id": 754,
            "marking": "Соединитель не определен",
            "modelPart": 4
          }
        ],
        "thickness": 58
      },
      "profileSystemName": "KBE Эталон 58",
      "square": 2.64,
      "userParameters": [
        {"hex": "", "id": 822, "title": "Толщина армирования", "value": "Авто"}
      ],
      "weight": 84.49
    }
  ],
  "quantity": 1,
  "square": 2.64,
  "totalCost": 3086000,
  "weight": 84.49
};
