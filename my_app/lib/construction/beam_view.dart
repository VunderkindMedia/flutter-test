import 'dart:ui';
import 'package:flame/components.dart';
import 'package:my_app/models/beam.dart';
import 'construction_view.dart';

class BeamView extends PositionComponent
    with Tappable, HasGameRef<Constructions> {
  //Компонент, представляющий собой балку рамы (в т.ч. импост)
  // @override
  // bool debugMode = true;
  bool _onPressed = false;
  Beam beam;
  BeamView({required this.beam});

  @override
  bool onTapDown(info) {
    _onPressed = !_onPressed;
    return true;
  }

  @override
  void render(Canvas canvas) {
    final painterFill = Paint()
      ..color = _onPressed ? const Color(0xff30C2FF) : const Color(0xffffffff)
      ..strokeWidth = 1
      ..style = PaintingStyle.fill;
    final painterStroke = Paint()
      ..color = const Color(0xff000000)
      ..strokeWidth = 1
      ..style = PaintingStyle.stroke;
    final outerPath = Path();

    outerPath.moveTo(
        beam.innerDrawingPoints[0].x, beam.innerDrawingPoints[0].y);

    beam.innerDrawingPoints.asMap().forEach((index, point) {
      if (index != 0) {
        outerPath.lineTo(point.x, point.y);
      }
    });
    outerPath.lineTo(
        beam.innerDrawingPoints[0].x, beam.innerDrawingPoints[0].y);
    outerPath.close();

    canvas.drawPath(outerPath, painterFill);
    canvas.drawPath(outerPath, painterStroke);
    super.render(canvas);
  }
}
