import 'dart:ui';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';
import 'package:my_app/models/construction.dart';
import 'product_view.dart';

class ConstructionDrawer extends StatelessWidget {
  final double scale;
  final double width;
  final double height;
  final Construction construction;
  const ConstructionDrawer(
      {Key? key,
      required this.width,
      required this.height,
      required this.scale,
      required this.construction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GameWidget(
      game: Constructions(
          viewportResolution: Vector2(width, height),
          scale: scale,
          construction: construction),
    );
  }
}

class Constructions extends FlameGame with HasTappableComponents {
  //Компонент, представляющий одну конструкцию
  final Vector2 viewportResolution;
  final double scale;
  Vector2? lastScale;
  Vector2? dragDelta;
  Construction construction;
  Constructions(
      {required this.viewportResolution,
      required this.scale,
      required this.construction});

  @override
  Color backgroundColor() => const Color(0xFFF2F2F2);

  @override
  void update(double dt) {
    camera.zoom = scale;

    super.update(dt);
  }

  @override
  Future<void> onLoad() async {
    await super.onLoad();
    construction.update();
    camera.viewport = FixedResolutionViewport(size);
    camera.setRelativeOffset(Anchor.center);

    camera.followVector2(Vector2(0, 0));
    camera.speed = 1;

    //Добавляем все изделия из конструкции
    for (var product in construction.products) {
      ProductView productView = ProductView(product: product);
      productView.flipVertically();
      add(productView);
    }
  }

  static const zoomPerScrollUnit = 0.1;
}
