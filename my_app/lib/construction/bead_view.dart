import 'dart:ui';
import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:my_app/models/polygon.dart';

class BeadView extends PositionComponent {
  @override
  bool debugMode = true;
  Polygon bead;
  BeadView({required this.bead});

  @override
  void render(Canvas canvas) {
    super.render(canvas);

    final painterStroke = Paint()
      ..color = const Color(0xff000000)
      ..strokeWidth = 1
      ..style = PaintingStyle.stroke;
    final outerPath = Path();
    Vector2 startPoint = bead.segments[0].start.toVector2();
    outerPath.moveTo(startPoint.x, startPoint.y);
    bead.segments.forEach((segment) {
      outerPath.lineTo(segment.end.x, segment.end.x);
    });
    outerPath.lineTo(startPoint.x, startPoint.y);
    outerPath.close();

    canvas.drawPath(outerPath, painterStroke);
  }
}
