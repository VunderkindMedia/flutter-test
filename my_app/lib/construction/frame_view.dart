import 'dart:math';
import 'package:flame/components.dart';
import 'package:my_app/construction/beam_view.dart';
import 'package:my_app/helpers/beam.dart';
import 'package:my_app/models/circular_linked_list.dart';
import 'package:my_app/models/frame_filling.dart';
import 'package:my_app/models/point.dart';

class FrameView extends PositionComponent {
  @override
  bool debugMode = true;
  FrameFilling frame;
  FrameView({required this.frame});

  @override
  Future<void> onLoad() async {
    for (var object in CircularLinkedList(frame.frameBeams).items) {
      final item = object.data;
      final prev = object.prev;
      final next = object.next;

      final itemLength = sqrt(
          ((item.end.x - item.start.x) * (item.end.x - item.start.x)) +
              ((item.end.y - item.start.y) *
                  (item.end.y - item.start.y))); //Длина текущей балки
      // final prevLength = sqrt(
      //     ((prev.end.x - prev.start.x) * (prev.end.x - prev.start.x)) +
      //         ((prev.end.y - prev.start.y) * (prev.end.y - prev.start.y))); //Длина предыдущей балки
      // final thirdLength = sqrt(
      //     ((item.end.x - prev.start.x) * (item.end.x - prev.start.x)) +
      //         ((item.end.y - prev.start.y) * (item.end.y - prev.start.y))); //Длина следующей балки

      final angle = atan2(item.start.y - item.end.y,
          item.start.x - item.end.x); //Угол балки по отношению к Оси X

      // List<Point> points = getPoints(
      //     item, prev, next, 45.0, 45.0, 45.0); //Точки для отрисовки балки

      final beamView = BeamView(beam: item)
        ..position = item.start
            .toVector2() //Позиция с которой начинается отрисовка контейнера компонента (равна стартовой точке балки)
        ..anchor = Anchor
            .bottomRight //Все балки рисуются с правого нижнего угла но поворачиваются относительно этой точки с помощью указания угла
        ..angle =
            angle //Собственно угол, на который балка поворачивается относительно оси X
        ..size = Vector2(itemLength,
            45) //Длина балки и ее ширина (ширину пока задал хардкодом, вообще она берется из профиля, который тоже есть в JSON)
        ..height = 75
        ..flipVertically()
        ..width = itemLength;

      add(beamView);
    }

    // for (var object in CircularLinkedList(frame.impostBeams).items) {
    //   final item = object.data;
    //   final prev = object.prev;
    //   final next = object.next;

    //   final itemLength = sqrt(
    //       ((item.end.x - item.start.x) * (item.end.x - item.start.x)) +
    //           ((item.end.y - item.start.y) * (item.end.y - item.start.y)));
    //   // final prevLength = sqrt(
    //   //     ((prev.end.x - prev.start.x) * (prev.end.x - prev.start.x)) +
    //   //         ((prev.end.y - prev.start.y) * (prev.end.y - prev.start.y)));
    //   // final thirdLength = sqrt(
    //   //     ((item.end.x - prev.start.x) * (item.end.x - prev.start.x)) +
    //   //         ((item.end.y - prev.start.y) * (item.end.y - prev.start.y)));

    //   final angle = atan2(item.start.y - item.end.y, item.start.x - item.end.x);

    //   List<Point> points = getPoints(item, prev, next, 45.0, 45.0, 45.0);

    //   final beamView = BeamView(beam: item, points: points)
    //     ..position = item.start.toVector2()
    //     ..anchor = Anchor.bottomRight
    //     ..angle = angle
    //     ..size = Vector2(itemLength, 45)
    //     ..height = 75
    //     ..flipVertically()
    //     ..width = itemLength;

    //   add(beamView);
    // }
  }
}
