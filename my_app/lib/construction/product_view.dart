import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/input.dart';
import 'package:my_app/construction/frame_view.dart';
import 'package:my_app/models/product.dart';

class ProductView extends PositionComponent with Tappable {
  late Product product;
  ProductView({required this.product});

  bool rendered = false;
  Vector2? dragDelta;

  double constructionOffsetX = 0;
  double constructionOffsetY = 0;
  double beamWidth = 40;
  @override
  bool onTapDown(info) {
    print('tap');
    return true;
  }

  @override
  onLoad() async {
    // product.frame.impostBeams.asMap().forEach((index, beam) {
    //   add(Frame(beam: beam)..position = Vector2(beam.start.x, beam.start.y));
    // });
    // product.frame.frameBeams.asMap().forEach((index, beam) {
    //   final height = beam.start.y - beam.end.y != 0
    //       ? (beam.start.y - beam.end.y).abs()
    //       : beam.width;

    //   final width = beam.start.x - beam.end.x != 0
    //       ? (beam.start.x - beam.end.x).abs()
    //       : beam.width;

    //   Anchor beamAnchor = Anchor.topLeft;

    //   Vector2 beamSize = Vector2(0, 0);

    //   if (index == 0) {
    //     beamSize = Vector2(-width, height);
    //   } else if (index == 1) {
    //     beamSize = Vector2(width, height);
    //   } else if (index == 2) {
    //     beamSize = Vector2(width, -height);
    //   } else if (index == 3) {
    //     beamSize = Vector2(-width, -height);
    //   }

    //   add(Frame(beam: beam)
    //     ..position = beam.start.toVector2()
    //     ..size = beamSize
    //     ..anchor = beamAnchor);
    // });

    // product.frame.solidFillings.forEach((element) {
    //   element.beads.forEach((bead) {
    //     add(BeadView(bead: bead)
    //       ..position = bead.segments[0].start.toVector2());
    //   });
    // });

    add(FrameView(frame: product.frame)
      ..position = position
      ..size = size);
    super.onLoad();
  }
}
