import 'package:my_app/constants/geometry_params.dart';
import 'package:my_app/helpers/guid.dart';
import 'package:my_app/models/circular_linked_list.dart';
import 'package:my_app/models/filling.dart';
import 'package:my_app/models/filling_size.dart';
import 'package:my_app/models/point.dart';
import 'package:my_app/models/polygon.dart';
import 'package:my_app/models/rectangle.dart';
import 'package:my_app/models/segment.dart';

class SolidFilling {
  String solidFillingGuid = GUID.generate();
  late String fillingVendorCode;
  late Point fillingPoint;
  late Filling? filling;
  late FillingSize? fillingSize;

  late String _parentNodeId;
  late Polygon _containerPolygon = Polygon();

  SolidFilling();

  set containerPolygon(Polygon polygon) {
    _containerPolygon = polygon;
    fillingPoint = polygon.center;
  }

  Polygon get containerPolygon {
    return _containerPolygon;
  }

  Rect get rect {
    return containerPolygon.rect;
  }

  String get nodeId {
    return solidFillingGuid;
  }

  set nodeId(String nodeId) {
    solidFillingGuid = nodeId;
  }

  String get parentNodeId {
    return _parentNodeId;
  }

  set parentNodeId(String parentNodeId) {
    _parentNodeId = parentNodeId;
  }

  List<Polygon> get beads {
    final List<Polygon> beads = [];
    List<CircularLinkedListItem<Segment>> items =
        CircularLinkedList(containerPolygon.innerPolygon.segments).items;
    for (var item in items) {
      final currentLine = item.data;
      final prevLine = item.prev;
      final nextLine = item.next;

      final currentBeamParallel = currentLine.getParallels(BEAD_WIDTH)[0];
      final prevBeamParallel = prevLine.getParallels(BEAD_WIDTH)[0];
      final nextBeamParallel = nextLine.getParallels(BEAD_WIDTH)[0];

      final point1 =
          currentBeamParallel.findIntersectionPoint(nextBeamParallel);
      final point2 =
          currentBeamParallel.findIntersectionPoint(prevBeamParallel);

      if (point1 == null || point2 == null) {
        continue;
      }

      beads.add(Polygon.buildFromPoints(
          [currentLine.start, currentLine.end, point1, point2]));
    }

    return beads;
  }

  factory SolidFilling.fromJson(Map<String, dynamic> json) {
    return SolidFilling()
      ..fillingVendorCode = json['fillingVendorCode'] as String
      ..fillingPoint = Point.fromJson(json['fillingPoint'])
      ..filling = Filling.fromJson(json['filling'])
      ..fillingSize = FillingSize.fromJson(json['fillingSize'])
      ..fillingPoint = Point.fromJson(json['fillingPoint']);
  }
}
