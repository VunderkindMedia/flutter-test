class ModelParts {
  static const Na = 0;
  static const Rama = 1;
  static const Stvorka = 2;
  static const Impost = 3;
  static const Connector = 4;
  static const Porog = 5;
  static const Shtulp = 6;
  static const Shtapik = 7;
  static const Falsh = 8;
  static const Zapolneniya = 9;
  static const Adapter = 10;
}
