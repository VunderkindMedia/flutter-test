class Filling {
  late num id;
  late String name;
  late String marking;
  late num thickness;
  late num camernost;
  late String group;
  late bool isLowEmission;
  late bool isMultifunctional;
  late bool isDefault;
  Filling();

  bool get isEnergy {
    return isLowEmission || isMultifunctional;
  }

  Filling get clone {
    return Filling.fromJson(toJson()); //Тут превратить в JSON и обратно
  }

  factory Filling.fromJson(Map<String, dynamic> json) {
    return Filling()
      ..id = json['id'] as num
      ..camernost = json['camernost'] as num
      ..group = json['group'] as String
      ..isLowEmission = json['isLowEmission'] as bool
      ..isMultifunctional = json['isMultifunctional'] as bool
      ..marking = json['marking'] as String
      ..name = json['name'] as String
      ..thickness = json['thickness'] as num;
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'camernost': camernost,
        'group': group,
        'isLowEmission': isLowEmission,
        'isMultifunctional': isMultifunctional,
        'marking': marking,
        'name': name,
        'thickness': thickness
      };
}
