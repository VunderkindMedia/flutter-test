import 'package:my_app/helpers/number.dart';
import 'package:my_app/models/line.dart';

import 'point.dart';

class Segment extends Line {
  Segment(Point start, Point end, {double width = 0})
      : super(start, end, width: width);

  bool hasPoint(Point point) {
    try {
      bool hasPoint(double start, double end, double coord) {
        return (((approximatelyEqual(start, coord) || start <= coord) &&
                (approximatelyEqual(coord, end) || coord <= end)) ||
            ((approximatelyEqual(end, coord) || end <= coord) &&
                (approximatelyEqual(coord, start) || coord <= start)));
      }

      if (isVertical && approximatelyEqual(point.x, start.x)) {
        return hasPoint(start.y, end.y, point.y);
      }

      if (hasPoint(start.x, end.x, point.x)) {
        return approximatelyEqual(
            (point.x - start.x) * (end.y - start.y) -
                (point.y - start.y) * (end.x - start.x),
            0);
      }

      return false;
    } catch (e) {
      return false;
    }
  }

  bool hasCommonStartOrEnd(Segment segment) {
    return hasPoint(segment.start) ||
        hasPoint(segment.end) ||
        segment.hasPoint(start) ||
        segment.hasPoint(end);
  }

  Point? findCommonPoint(Segment segment) {
    Point? intersectionPoint = findIntersectionPoint(segment);

    if (intersectionPoint != null &&
        hasPoint(intersectionPoint) &&
        segment.hasPoint(intersectionPoint)) {
      return intersectionPoint;
    }

    return null;
  }

  Segment? findCommonSegment(Segment segment) {
    if (hasPoint(segment.start) && hasPoint(segment.end)) {
      return Segment(segment.start, segment.end);
    }

    if (segment.hasPoint(start) && segment.hasPoint(end)) {
      return Segment(start, end);
    }

    if (hasPoint(segment.start)) {
      Point point1 = segment.start;
      Point point2 = segment.hasPoint(start) ? start : end;
      return Segment(point1, point2);
    }

    if (hasPoint(segment.end)) {
      Point point1 = segment.hasPoint(start) ? start : end;
      Point point2 = segment.end;
      return Segment(point1, point2);
    }

    return null;
  }

  @override
  Segment withOffset(double offsetX, double offsetY) {
    Line line = super.withOffset(offsetX, offsetY);
    Segment segment = Segment(line.start, line.end);
    segment.width = width;
    return segment;
  }

  factory Segment.fromJson(Map<String, dynamic> json) {
    return Segment(json['start'], json['end'], width: json['width']);
  }
}
