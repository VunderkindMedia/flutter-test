import 'dart:math';

import 'package:my_app/helpers/number.dart';
import 'package:my_app/models/size.dart';

import 'point.dart';
import 'rectangle.dart';

class Line {
  late Point start = Point(x: 0, y: 0);
  late Point end = Point(x: 0, y: 0);
  late double width = 0;
  Line(this.start, this.end, {this.width = 45});

  static Line get abscissa {
    return Line(Point.zero(), Point(x: 1, y: 0));
  }

  static Line get ordinate {
    return Line(Point.zero(), Point(x: 0, y: 1));
  }

  double get length {
    return start.distanceTo(end);
  }

  bool get isVertical {
    return start.x == end.x;
  }

  bool get isHorizontal {
    return start.y == end.y;
  }

  Point? findIntersectionPoint(Line line) {
    print('y ${line.start.y}');
    try {
      double x1 = start.x; //Координаты балки
      double y1 = start.y;
      double x2 = end.x;
      double y2 = end.y;

      double x3 = line.start.x; //Координаты условной оси X
      double y3 = line.start.y;
      double x4 = line.end.x;
      double y4 = line.end.y;

      double a = (x1 * y2) - (y1 * x2); //тангенс угла между прямой и осью X
      double b = x3 * y4 - y3 * x4; //тангенс угла между прямой и осью X

      double x = (a * (x3 - x4) - (x1 - x2) * b) /
          ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
      double y = (a * (y3 - y4) - (y1 - y2) * b) /
          ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

      if (x.isNaN || x.isInfinite || y.isNaN || y.isInfinite) {
        return null;
      }

      return Point(x: x, y: y);
    } catch (e) {
      return null;
    }
  }

  List<Point> findIntersectionPoints(List<Line> lines) {
    List<Point> points = [];

    lines.forEach((line) {
      Point? point = line.findIntersectionPoint(this);
      if (point != null) points.add(point);
    });

    return points;
  }

  double findIntersectionAngle(Line line) {
    var line1Start = start;
    var line1End = end;
    var line2Start = line.start;
    var line2End = line.end;

    Point? intersectionPoint = findIntersectionPoint(line);

    if (intersectionPoint == null) {
      return 0;
    }

    if (intersectionPoint.equals(end)) {
      line1Start = end;
      line1End = start;
    }
    if (intersectionPoint.equals(line.end)) {
      line2Start = line.end;
      line2End = line.start;
    }

    double x1 = line1End.x - line1Start.x;
    double y1 = line1End.y - line1Start.y;

    double x2 = line2End.x - line2Start.x;
    double y2 = line2End.y - line2Start.y;

    double cosFi = (x1 * x2 + y1 * y2) /
        (sqrt(x1 * x1 + y1 * y1) * sqrt(x2 * x2 + y2 * y2));

    return radiansToDegrees(acos(cosFi));
  }

  double get angle {
    if (isHorizontal) {
      return 0;
    }

    final bottomostPoint = start.y < end.y ? start : end;

    return Line(bottomostPoint, bottomostPoint.withOffset(1, 0))
        .findIntersectionAngle(this);
  }

  List<Line> getParallels(double d) {
    // a*x' + b*y' + c +- d*sqrt(a*a + b*b) = 0

    double intersectionAngle =
        findIntersectionAngle(Line(Point.zero(), Point(x: 2000, y: 0)));

    double A = start.y - end.y; //Высота
    double B = end.x - start.x; //Ширина
    double C = start.x * end.y - end.x * start.y;

    if (intersectionAngle <= 45) {
      double yStart1 = (-d * sqrt(A * A + B * B) - A * start.x - C) / B;
      double yEnd1 = (-d * sqrt(A * A + B * B) - A * end.x - C) / B;

      double yStart2 = (d * sqrt(A * A + B * B) - A * start.x - C) / B;
      double yEnd2 = (d * sqrt(A * A + B * B) - A * end.x - C) / B;

      // в первую очередь отдаем внутренние паралели
      return [
        Line(Point(x: start.x, y: yStart1), Point(x: end.x, y: yEnd1)),
        Line(Point(x: start.x, y: yStart2), Point(x: end.x, y: yEnd2))
      ];
    } else {
      double xStart1 = (-d * sqrt(A * A + B * B) - B * start.y - C) / A;
      double xEnd1 = (-d * sqrt(A * A + B * B) - B * end.y - C) / A;

      double xStart2 = (d * sqrt(A * A + B * B) - B * start.y - C) / A;
      double xEnd2 = (d * sqrt(A * A + B * B) - B * end.y - C) / A;

      // в первую очередь отдаем внутренние паралели
      return [
        Line(Point(x: xStart1, y: start.y), Point(x: xEnd1, y: end.y)),
        Line(Point(x: xStart2, y: start.y), Point(x: xEnd2, y: end.y))
      ];
    }
  }

  Line withOffset(double offsetX, double offsetY) {
    return Line(Point(x: start.x + offsetX, y: start.y + offsetY),
        Point(x: end.x + offsetX, y: end.y + offsetY));
  }

  Rect get rect {
    return Rect.buildFromPoints([start, end]);
  }

  Line get OX {
    Point refPoint = rect.refPoint;
    Size size = rect.size;
    return Line(
        Point(x: refPoint.x, y: 0), Point(x: refPoint.x + size.width, y: 0));
  }

  Line get OY {
    Point refPoint = rect.refPoint;
    Size size = rect.size;
    return Line(
        Point(x: 0, y: refPoint.y), Point(x: 0, y: refPoint.y + size.height));
  }

  factory Line.fromJson(Map<String, dynamic> json) {
    return Line(json['start'], json['end'], width: json['width']);
  }
}
