import 'frame_filling.dart';

class Product {
  ///ID типа изделия
  late int productionTypeId;

  /// ID типа конструкции
  late int constructionTypeId;
  late FrameFilling frame;

  Product({
    this.productionTypeId = 0,
    this.constructionTypeId = 0,
  });

  update() {
    // this.profileSystemName = this.profile.name
    // this.hardwareSystemName = this.furniture.name
    // this._updateConnectors()
    frame.update();
  }

  factory Product.empty() {
    final construction = Product();
    construction
      ..productionTypeId = 0
      ..constructionTypeId = 0;

    return construction;
  }

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product()
      ..productionTypeId = json['productionTypeId'] as int
      ..constructionTypeId = json['constructionTypeId'] as int
      ..frame = FrameFilling.fromJson(json['frame']);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'productionTypeId': productionTypeId,
        'constructionTypeId': constructionTypeId,
        'frame': frame
      };
}
