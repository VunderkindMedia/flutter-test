import 'dart:math';

import 'package:flame/components.dart';

class Point {
  double x;
  double y;
  Point({
    required this.x,
    required this.y,
  });

  factory Point.zero() {
    return Point(x: 0, y: 0);
  }
  Vector2 toVector2() {
    return Vector2(x, y);
  }

  bool equals(Point point) {
    return x == point.x && y == point.y;
  }

  double distanceTo(Point point) {
    return sqrt(pow(x - point.x, 2) + pow(y - point.y, 2));
  }

  get clone {
    return Point(x: x, y: y);
  }

  Point withOffset(num offsetX, num offsetY) {
    return Point(x: x + offsetX, y: y + offsetY);
  }

  Point findNearestPoint(List<Point> points) {
    if (points.isEmpty) {
      throw Exception('Нужно передать хотяб одну точку');
    }

    Point foundedPoint = points[0];
    var r = double.infinity;

    points.forEach((p) {
      final d = distanceTo(p);
      if (d < r) {
        r = d;
        foundedPoint = p;
      }
    });

    return foundedPoint;
  }

  factory Point.fromJson(Map<String, dynamic> json) {
    return Point(
      x: json['x'].toDouble() as double,
      y: json['y'].toDouble() as double,
    );
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'x': x,
        'y': y,
      };
}
