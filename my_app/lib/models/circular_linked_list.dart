class CircularLinkedListItem<T> {
  final T data;
  final T prev;
  final T next;
  final num index;

  CircularLinkedListItem(
      {required this.data,
      required this.prev,
      required this.next,
      required this.index});
}

class CircularLinkedList<T> {
  List<CircularLinkedListItem<T>> items = [];

  CircularLinkedList(List<T> listItems) {
    if (listItems.length < 3) {
      throw Exception('Required 3 items, but got ${listItems.length}');
    }
    listItems.sublist(0).forEach((item) {
      int index = listItems.sublist(0).indexOf(item);
      final prev =
          index == 0 ? listItems[listItems.length - 1] : listItems[index - 1];
      final next =
          index == listItems.length - 1 ? listItems[0] : listItems[index + 1];
      items.add(CircularLinkedListItem<T>(
          data: item, prev: prev, next: next, index: index));
    });
  }
}
