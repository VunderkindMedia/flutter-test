class FillingSizeParams {
  late double x;
  late double y;

  FillingSizeParams();
  factory FillingSizeParams.fromJson(Map<String, dynamic> json) {
    return FillingSizeParams()
      ..x = json['x'].toDouble() as double
      ..y = json['y'].toDouble() as double;
  }
}

class FillingSize {
  late FillingSizeParams size;
  late List<FillingSizeParams> bevels;

  FillingSize();

  factory FillingSize.fromJson(Map<String, dynamic> json) {
    return FillingSize()
      ..size = FillingSizeParams.fromJson(json['size'])
      ..bevels = json['bevels']!
          .map<FillingSizeParams>((e) => FillingSizeParams.fromJson(e))
          .toList() as List<FillingSizeParams>;
  }
}
