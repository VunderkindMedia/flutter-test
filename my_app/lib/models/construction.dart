import 'package:my_app/models/product.dart';

class Construction {
  List<Product> products = [];

  Construction();

  update() {
    for (Product item in products) {
      item.update();
    }
  }

  factory Construction.fromJson(Map<String, dynamic> json) {
    return Construction()
      ..products = json['products']!
          .map<Product>((e) => Product.fromJson(e))
          .toList() as List<Product>;
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'products': products,
      };
}
