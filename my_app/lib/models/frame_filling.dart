import 'package:my_app/helpers/guid.dart';
import 'package:my_app/models/circular_linked_list.dart';
import 'package:my_app/models/inner_filling.dart';
import 'package:my_app/models/point.dart';
import 'package:my_app/models/solid_filling.dart';

import 'beam.dart';
import 'line.dart';
import 'model_parts.dart';

class FrameFilling {
  String frameFillingGuid = GUID.generate();
  late List<Beam> frameBeams = [];
  late List<Beam> impostBeams =
      []; // Пока импосты берем как Beam потом надо создать расширенный класс
  late Point fillingPoint;
  late List<InnerFilling> innerFillings = [];
  FrameFilling();

  factory FrameFilling.fromJson(Map<String, dynamic> json) {
    return FrameFilling()
      ..frameBeams = json['frameBeams']!
          .map<Beam>((e) => Beam.fromJson(e))
          .toList() as List<Beam>
      ..impostBeams = json['impostBeams']!
          .map<Beam>((e) => Beam.fromJson(e))
          .toList() as List<Beam>
      ..innerFillings = json['innerFillings']!
          .map<InnerFilling>((e) => InnerFilling.fromJson(e))
          .toList() as List<InnerFilling>
      ..fillingPoint = Point.fromJson(json['fillingPoint']);
  }

  List<SolidFilling> get solidFillings {
    List<SolidFilling> solidFillings = [];
    for (InnerFilling filling in innerFillings) {
      if (filling.solid != null) {
        solidFillings.add(filling.solid!);
      }
    }
    return solidFillings;
  }

  CircularLinkedList<Beam> get beamsCircularLinkedList {
    return CircularLinkedList(frameBeams);
  }

  void _updateFrameBeamsDrawingPoints() {
    List<Point> getPoints(Beam currentBeam, Beam prevBeam, Beam nextBeam,
        double currentBeamWidth, double prevBeamWidth, double nextBeamWidth) {
      List<Point> points = [];

      Line currentBeamParallel = currentBeam.getParallels(currentBeamWidth)[0];
      Line prevBeamParallel = prevBeam.getParallels(prevBeamWidth)[0];
      Line nextBeamParallel = nextBeam.getParallels(nextBeamWidth)[0];

      if (currentBeam.modelPart == ModelParts.Porog) {
        Point? point1 = currentBeamParallel.findIntersectionPoint(nextBeam);
        Point? point2 = currentBeamParallel.findIntersectionPoint(prevBeam);

        if (point1 != null && point2 != null) {
          points.add(currentBeam.start);
          points.add(currentBeam.end);
          points.add(point1);
          points.add(point2);
        }
      } else if (prevBeam.modelPart == ModelParts.Porog) {
        Point? point1 = currentBeamParallel.findIntersectionPoint(nextBeam);
        Point? point2 =
            currentBeamParallel.findIntersectionPoint(prevBeamParallel);
        Point? point3 = currentBeam.findIntersectionPoint(prevBeamParallel);

        if (point1 != null && point2 != null && point3 != null) {
          points.add(currentBeam.end);
          points.add(point1);
          points.add(point2);
          points.add(point3);
        }
      } else if (nextBeam.modelPart == ModelParts.Porog) {
        Point? point1 = currentBeam.findIntersectionPoint(nextBeamParallel);
        Point? point2 =
            currentBeamParallel.findIntersectionPoint(nextBeamParallel);
        Point? point3 =
            currentBeamParallel.findIntersectionPoint(prevBeamParallel);

        if (point1 != null && point2 != null && point3 != null) {
          points.add(currentBeam.start);
          points.add(point1);
          points.add(point2);
          points.add(point3);
        }
      } else {
        Point? point1 =
            currentBeamParallel.findIntersectionPoint(nextBeamParallel);
        Point? point2 =
            currentBeamParallel.findIntersectionPoint(prevBeamParallel);
        if (point1 != null && point2 != null) {
          points.add(currentBeam.start);
          points.add(currentBeam.end);
          points.add(point1);
          points.add(point2);
        }
      }

      return points.length != 4 ? [] : points;
    }

    beamsCircularLinkedList.items.forEach((item) {
      Beam currentBeam = item.data;
      Beam prevBeam = item.prev;
      Beam nextBeam = item.next;

      currentBeam.innerDrawingPoints = getPoints(
          currentBeam,
          prevBeam,
          nextBeam,
          currentBeam.innerWidth,
          prevBeam.innerWidth,
          nextBeam.innerWidth);
      // currentBeam.outerDrawingPoints = getPoints(currentBeam, prevBeam, nextBeam, currentBeam.width, prevBeam.width, nextBeam.width)
    });
  }

  update() {
    // this._defineConnections()
    // this._updateBeamsAndImpostsGeometryParams()
    _updateFrameBeamsDrawingPoints();
    // this._updateImpostsAndInnerFillings()
  }

  List<FrameFilling> get leafFillings {
    List<FrameFilling> leafFillings = [];
    for (InnerFilling filling in innerFillings) {
      if (filling.leaf != null) {
        leafFillings.add(filling.leaf!);
      }
    }
    return leafFillings;
  }
}
