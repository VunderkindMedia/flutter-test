import 'package:my_app/constants/geometry_params.dart';
import 'package:my_app/helpers/guid.dart';
import 'package:my_app/models/point.dart';
import 'package:my_app/models/segment.dart';

import 'model_parts.dart';

class Beam extends Segment {
  String beamGuid = GUID.generate();

  late String profileVendorCode;
  late num modelPart;
  late List<Point> _innerDrawingPoints;
  late bool _hasInnerWidth = false;

  List<Point> get innerDrawingPoints {
    return _innerDrawingPoints;
  }

  set innerDrawingPoints(List<Point> points) {
    _innerDrawingPoints = points;
  }

  bool get hasInnerWidth {
    return _hasInnerWidth;
  }

  set hasInnerWidth(bool hasInnerWidth) {
    _hasInnerWidth = hasInnerWidth;
  }

  double get innerWidth {
    if (!hasInnerWidth) {
      return width;
    }
    if (modelPart == ModelParts.Porog) {
      return width;
    }
    if (modelPart == ModelParts.Impost || modelPart == ModelParts.Shtulp) {
      return width - 2 * BEAD_WIDTH;
    }

    return width - BEAD_WIDTH;
  }

  Beam(Point start, Point end,
      {double width = 45}) //Ширину пока захардкодил, позже возьмем из профиля
      : super(start, end, width: width);

  factory Beam.fromJson(Map<String, dynamic> json) {
    return Beam(Point.fromJson(json['start']), Point.fromJson(json['end']))
      ..modelPart = json['modelPart'];
  }
}
