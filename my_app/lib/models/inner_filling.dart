import 'package:my_app/models/solid_filling.dart';

import 'frame_filling.dart';

class InnerFilling {
  SolidFilling? solid;
  FrameFilling? leaf;

  InnerFilling({this.solid, this.leaf});

  factory InnerFilling.fromJson(Map<String, dynamic> json) {
    return InnerFilling()
      ..solid =
          json['solid'] != null ? SolidFilling.fromJson(json['solid']) : null
      ..leaf =
          json['leaf'] != null ? FrameFilling.fromJson(json['leaf']) : null;
  }
}
