import 'package:my_app/constants/geometry_params.dart';
import 'package:my_app/helpers/line.dart';
import 'package:my_app/helpers/point.dart';
import 'package:my_app/helpers/safe_while.dart';
import 'package:collection/collection.dart';
import 'package:my_app/models/circular_linked_list.dart';
import 'package:my_app/models/point.dart';
import 'rectangle.dart';
import 'segment.dart';

class Polygon {
  String? _id;
  late List<Segment> _segments;
  late List<Polygon> childrenPolygons;

  Polygon([List<Segment> segments = const []]) {
    if (segments.isNotEmpty) {
      Segment? bottomSegment = getBottommostLine(segments);
      List<Segment?> orderedSegments = [bottomSegment];

      if (bottomSegment != null) {
        safeWhile(() => orderedSegments.length != segments.length, () {
          Segment? lastSegment = orderedSegments[orderedSegments.length - 1];
          Segment? nextSegment = segments.firstWhereOrNull((Segment segment) =>
              lastSegment != null
                  ? segment.start.equals(lastSegment.end)
                  : false);
          if (nextSegment != null) {
            orderedSegments.add(nextSegment);
          }
        });
      }

      segments = orderedSegments
          .where((Segment? s) => s != null)
          .map((Segment? s) => Segment(s!.start, s.end, width: s.width))
          .toList();
    }
  }

  static Polygon buildFromPoints(List<Point> points) {
    CircularLinkedList<Point> pointsCircularLinkedList =
        CircularLinkedList(points);
    List<Segment> segments = [];
    for (var item in pointsCircularLinkedList.items) {
      segments.add(Segment(item.data, item.next));
    }

    return Polygon(segments);
  }

  List<Point> get points {
    List<Point> points = [];
    for (var segment in segments) {
      points.add(segment.start);
    }
    return points;
  }

  Point get center {
    return getCenter(points);
  }

  Rect get rect {
    return Rect.buildFromPoints(points);
  }

  List<Segment> get segments {
    return _segments;
  }

  set segments(List<Segment> segments) {
    _segments = segments;
  }

  List<Segment> get innerSegments {
    final List<Segment> segments = [];

    for (var item in CircularLinkedList(segments
            .where((segment) => segment.length > APPROXIMATE_BEAM_WIDTH)
            .toList())
        .items) {
      final currentSegment = item.data;
      final prevSegment = item.prev;
      final nextSegment = item.next;

      final currentBeamParallel =
          currentSegment.getParallels(currentSegment.width)[0];
      final prevBeamParallel = prevSegment.getParallels(prevSegment.width)[0];
      final nextBeamParallel = nextSegment.getParallels(nextSegment.width)[0];

      final point1 =
          currentBeamParallel.findIntersectionPoint(prevBeamParallel);
      final point2 =
          currentBeamParallel.findIntersectionPoint(nextBeamParallel);

      if (point1 == null || point2 == null) {
        continue;
      }
      segments.add(Segment(point1, point2));
    }

    final List<int> needRemoveLinesIndexes = [];

    final items = CircularLinkedList(segments).items;
    for (var item in items) {
      int index = items.indexOf(item);
      final segment = item.data;

      if (segment.length < BEAD_WIDTH) {
        needRemoveLinesIndexes.add(index);

        final prevSegment = item.prev;
        final nextSegment = item.next;

        final intersectionPoint =
            prevSegment.findIntersectionPoint(nextSegment);
        if (intersectionPoint != null) {
          prevSegment.end = intersectionPoint;
          nextSegment.start = intersectionPoint;
        }
      }
    }

    return segments
        .where((segment) =>
            !needRemoveLinesIndexes.contains(segments.indexOf(segment)))
        .toList();
  }

  Polygon get innerPolygon {
    return Polygon(innerSegments);
  }

  factory Polygon.fromJson(Map<String, dynamic> json) {
    return Polygon()
      ..segments = json['_segments']
          .map<Segment>((e) => Segment.fromJson(e))
          .toList() as List<Segment>;
  }
}
