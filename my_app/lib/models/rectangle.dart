import 'dart:math';

import 'package:my_app/helpers/point.dart';
import 'package:my_app/models/point.dart';

import 'size.dart';

class Rect {
  static Rect buildFromPoints(List<Point> points) {
    List<double> xArray = points.map((point) => point.x).toList();
    List<double> yArray = points.map((point) => point.y).toList();

    double minX = xArray.reduce(min);
    double maxX = xArray.reduce(max);

    double minY = yArray.reduce(min);
    double maxY = yArray.reduce(max);

    return Rect(
        refPoint: Point(x: minX, y: minY),
        size: Size(width: maxX - minX, height: maxY - minY));
  }

  late Point refPoint = Point.zero();
  late Size size = Size(width: 0, height: 0);
  late String _id;

  String get id {
    return _id;
  }

  Point get center {
    return Point(
        x: refPoint.x + size.width / 2, y: refPoint.y + size.height / 2);
  }

  Point get rightTopPoint {
    return refPoint.withOffset(size.width, size.height);
  }

  Point get leftBottomPoint {
    return refPoint;
  }

  Point get rightBottomPoint {
    return refPoint.withOffset(size.width, 0);
  }

  Point get leftTopPoint {
    return refPoint.withOffset(0, size.height);
  }

  List<Point> get drawingPoints {
    return [
      Point(x: refPoint.x, y: refPoint.y),
      Point(x: refPoint.x + size.width, y: refPoint.y),
      Point(x: refPoint.x + size.width, y: refPoint.y + size.height),
      Point(x: refPoint.x, y: refPoint.y + size.height)
    ];
  }

  bool hasPointInside(Point point) {
    return (refPoint.x <= point.x &&
        point.x <= refPoint.x + size.width &&
        refPoint.y <= point.y &&
        point.y <= refPoint.y + size.height);
  }

  Rect({required this.refPoint, required this.size}) {
    _id = getIdFromPoints('rectangle', drawingPoints);
  }
}
