class Size {
  double width;
  double height;

  Size({required this.height, required this.width});

  bool equals(Size size, [double epsilon = 1]) {
    return (width - size.width).abs() <= epsilon &&
        (height - size.height).abs() <= epsilon;
  }

  Size scaled(double value) {
    return Size(width: width * value, height: height * value);
  }
}
