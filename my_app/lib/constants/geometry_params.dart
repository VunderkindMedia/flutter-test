// ignore_for_file: non_constant_identifier_names

double BEAD_WIDTH = 10;
double DEFAULT_B = 28;
double POROG_B = 8;
double ALUMINIUM_B = 4;

double MIN_WIDTH = 100;
double MIN_HEIGHT = 100;

double MAX_WIDTH = 2800;
double MAX_HEIGHT = 2500;

double MAX_BALCON_DOOR_WIDTH = 2500;
double MAX_BALCON_DOOR_HEIGHT = 2800;

double MAX_ALUMINIUM_WIDTH = 5950;
double MAX_ALUMINIUM_HEIGHT = 2500;

double APPROXIMATE_BEAM_WIDTH = 70;

double MIN_CONNECTOR_HEIGHT = 100;
